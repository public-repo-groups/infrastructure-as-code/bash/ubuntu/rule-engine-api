#!/bin/bash
echo "Start Install nginx for ubuntu"

apt install -y nginx

ufw app list
ufw allow 'Nginx HTTP'
ufw status

systemctl enable nginx
systemctl start nginx
systemctl status nginx

echo "End Install nginx for ubuntu"
